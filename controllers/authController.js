const { User } = require('../models/user');
const { Organization } = require('../models/organization');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
require('dotenv').config();

const createToken = (obj) => jwt.sign({
    id: obj._id
}, process.env.JWT_KEY, { expiresIn: '24h' });

exports.postLogin = async (req, res, next) => {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    if (user && passwordHash.verify(password, user.password)) {
        const token = createToken(user);
        return res.status(200).json({ confirm: true, message: "Użytkownik poprawnie zalogowany", user, token });
    }

    const organization = await Organization.findOne({ email });

    if (organization && passwordHash.verify(password, organization.password)) {
        const token = createToken(organization);
        return res.status(200).json({ confirm: true, message: "Organizacja poprawnie zalogowana", organization, token });
    }

    res.status(404).json({ confirm: true, message: "Błędny email lub hasło" });
}

exports.postTokenData = async (req, res, next) => {
    const { token } = req.body;

    try {
        const { id } = jwt.verify(token, process.env.JWT_KEY, null);

        const user = await User.findById(id);

        if(user) return res.status(200).json({ confirm: true, message: "Weryfikacja przebiegła pomyślnie", user });
        
        const organization = await Organization.findById(id);

        if(organization) return res.status(200).json({ confirm: true, message: "Weryfikacja przebiegła pomyślnie", organization });

        res.status(404).json({ confirm: true, message: "Błędny token" });
    } catch (error) {
        res.status(404).json({ confirm: true, message: "Błędny token" });
    }
}