const { Organization } = require('../models/organization');
const { User } = require('../models/user');
const passwordHash = require('password-hash');

exports.postAddOrganization = async (req, res, next) => {
    const existingOrganization = await Organization.findOne({ email: req.body.email });
    const existingUser = await User.findOne({ email: req.body.email });

    if (existingUser || existingOrganization) return res.status(409).json({ confirm: false, message: "Użytkownik o takiej nazwie już istnieje" });

    const password = passwordHash.generate(req.body.password);
    const { name, email } = req.body;

    const organization = await Organization.create({ email, password, name });

    res.status(201).json({ confirm: true, message: "Organizacja pomyślnie dodana", organization });
}

exports.patchOrganizationData = async (req, res, nest) => {
    const orgId = req.params.orgId;
    try {
        const updatedOrg = await Organization.findByIdAndUpdate(orgId, { ...req.body }, { new: true });
        res.status(200).json({ confirm: true, message: "Dane pomyślnie zaktualizowane", organization: updatedOrg });
    } catch (err) {
        res.status(409).json({ confirm: false, message: "Błąd przy aktualizacji danych" });
    }
}

exports.deleteOrganization = async (req, res, next) => {
    const orgId = req.params.orgId;

    try {
        await Organization.findByIdAndDelete(orgId)
        res.status(200).json({ confirm: true, message: "Pomyślnie usunięto organizację" })
    } catch (error) {
        res.status(409).json({ confirm: false, message: "Wystąpił błąd przy usuwaniu organizacji" })
    }
}

exports.putOrganizationOpinions = async (req, res, next) => {
    const orgId = req.params.orgId;
    try {
        const updatedOrg = await Organization.findByIdAndUpdate(orgId, { $push: { opinions: { ...req.body, date: new Date() } } }, { new: true });
        res.status(200).json({ confirm: true, message: "Pomyślnie dodano opinię", organization: updatedOrg });
    } catch (error) {
        res.status(409).json({ confirm: false, message: "Wystąpił błąd przy dodawaniu opinii" });
    }
}

exports.getOrganization = async (req, res, next) => {
    const orgId = req.params.orgId;

    try {
        const organization = await Organization.findById(orgId);
        if (!org) return res.status(404).json({ confirm: false, message: "Nie znaleziono organizacji" });
        res.status(200).json({ confirm: true, message: "Pomyślnie znaleziono organizację", organization });

    } catch (error) {
        res.status(404).json({ confirm: false, message: "Wystąpił błąd przy szukaniu organizacji" });
    }
}

exports.getOrganizations = async (req, res, next) => {
    try {
        const orgnizations = await Organization.find({});
        if (orgs.length <= 0) return res.status(404).json({ confirm: false, message: "Nie znaleziono organizacji" });
        res.status(200).json({ confirm: true, message: "Pomyślnie pobrano organizacje", orgnizations });
    } catch (err) {
        res.status(404).json({ confirm: false, message: "Wystąpił błąd przy szukaniu organizacji" });
    }
}