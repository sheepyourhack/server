const { Offer } = require('../models/offer');

const options = {
    page: 1,
    limit: 10
}

exports.postAddOffer = async (req, res, next) => {
    try {
        const offer = await Offer.create({ ...req.body });
        res.status(201).json({ confirm: true, message: "Oferta pomyślnie dodana", offer });
    } catch (error) {
        console.log(error)
        res.status(409).json({ confirm: false, message: "Błąd przy dodawaniu oferty" });
    }
}

exports.deleteOffer = async (req, res, next) => {
    const { offerId } = req.params;

    try {
        await Offer.findByIdAndDelete(offerId)
        res.status(200).json({ confirm: true, message: "Pomyślnie usunięto ofertę" })
    } catch (error) {
        res.status(409).json({ confirm: false, message: "Wystąpił błąd przy usuwaniu oferty" })
    }
}

exports.postOrganizationOffers = async (req, res, next) => {
    const { ownerId, page } = req.body;
    try {
        const offers = await Offer.paginate({ownerId}, page ? { page, limit: 10 } : options)
        res.status(200).json({ confirm: true, message: "Pomyślnie pobrano oferty", offers });
    } catch (error) {
        console.log(error);
        res.status(404).json({ confirm: false, message: "Wystąpił błąd przy szukaniu ofert" });
    }
}

exports.postOffers = async (req, res, next) => {
    const { page, searchText, city, ownerType, type } = req.body;

    try {
        // make regex string to search in database
        const words = searchText.replace(/[^a-zA-Z ]/g, "").split(" ");

        let regex = "^(?=.*";

        words.forEach((word, i) => {
            if (i !== words.length - 1)
                regex += word + ")(?=.*";
            else
                regex += word + ").*$";
        });

        const offers = await Offer.paginate({
            $and: [
                {
                    $or: [
                        { title: { $regex: regex, $options: "i" } },
                        { ownerName: { $regex: regex, $options: "i" } }
                    ]
                },
                { city: { $regex: city, $options: "i" } },
                { ownerType: ownerType },
                { type: { $in: type } }
            ]

        },
            page ? { page, limit: 10 } : options);

        if (offers.length <= 0) return res.status(404).json({ confirm: false, message: "Nie znaleziono ofert" });
        res.status(200).json({ confirm: true, message: "Pomyślnie pobrano oferty", offers });
    } catch (err) {
        console.log(err)
        res.status(404).json({ confirm: false, message: "Wystąpił błąd przy szukaniu ofert" });
    }
}

exports.getOffer = async (req, res, next) => {
    const offerId = req.params.offerId;

    try {
        const org = await Offer.findById(offerId);
        if (!org) return res.status(404).json({ confirm: false, message: "Nie znaleziono ofert" });
        res.status(200).json({ confirm: true, message: "Pomyślnie znaleziono ofertę", org });

    } catch (error) {
        res.status(404).json({ confirm: false, message: "Wystąpił błąd przy szukaniu oferty" });
    }
}