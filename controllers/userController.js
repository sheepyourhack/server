const { User } = require('../models/user');
const { Organization } = require('../models/organization');
const passwordHash = require('password-hash');

exports.postAddUser = async (req, res, next) => {
    const { email, firstName, lastName } = req.body;

    const password = passwordHash.generate(req.body.password);
    const birthDate = new Date(req.body.birthDate);

    const existingUser = await User.findOne({ email });
    const existingOrganization = await Organization.findOne({ email });

    if (existingUser || existingOrganization) return res.status(409).json({ confirm: false, message: "Użytkownik o takiej nazwie już istnieje" });

    const user = await User.create({ email, firstName, lastName, birthDate, password });

    res.status(201).json({ confirm: true, message: "Użytkownik pomyślnie dodany", user });
}

exports.patchUserData = async (req, res, next) => {
    const { userId } = req.params;

    try {
        const user = await User.findByIdAndUpdate(userId, { ...req.body }, { new: true });
        res.status(200).json({ confirm: true, message: "Dane pomyślnie zaktualizowane", user });
    } catch (err) {
        console.log(err);
        res.status(409).json({ confirm: false, message: "Błąd podczas aktualizacji danych" });
    }
}

exports.deleteUser = async (req, res, next) => {
    const { userId } = req.params;

    try {
        await User.findByIdAndDelete(userId);
        res.status(200).json({ confirm: true, message: "Użytkownik pomyślnie usunięty" });
    } catch (err) {
        res.status(409).json({ confirm: false, message: "Wystąpił błąd podczas usuwania użytkownika" });
    }
}

exports.putComment = async (req, res, next) => {
    const { userId } = req.params;

    try {
        const user = await User.findByIdAndUpdate(userId, { $push: { opinions: { ...req.body, date: new Date() } } }, { new: true });
        res.status(200).json({ confirm: true, message: "Dodano komentarz", user });
    } catch (err) {
        res.status(409).json({ confirm: false, message: "Wystąpił błąd podczas dodawania komentarza" });
    }
}

exports.getUser = async (req, res, next) => {
    const { userId } = req.params;

    try {
        const user = await User.findById(userId);

        if (!user) return res.status(404).json({ confirm: false, message: "Brak takiego użytkownika" });

        res.status(200).json({ confirm: true, message: "Dane użytkownika pomyślnie pobrane", user });
    } catch (err) {
        res.status(404).json({ confirm: false, message: "Wystąpił błąd podczas pobierania danych użytkownika" });
    }
}

exports.getUsers = async (req, res, next) => {
    try {
        const users = await User.find({});

        if (users.length <= 0) return res.status(404).json({ confirm: false, message: "Brak użytkowników" });

        res.status(200).json({ confirm: true, message: "Dane użytkowników pomyślnie pobrane", users });
    } catch (err) {
        res.status(404).json({ confirm: false, message: "Wystąpił błąd podczas pobierania danych użytkowników" });
    }
}