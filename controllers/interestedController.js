const { Interested } = require('../models/interested');

exports.postAddInterested = async (req, res, next) => {
    try {
        const interested = await Interested.create({ ...req.body, date: new Date() });
        res.status(201).json({ confirm: true, message: "Pomyślnie wysłano zainteresowanie", interested });
    } catch (error) {
        res.status(409).json({ confirm: false, message: "Błąd przy wysyłaniu zainteresowania" });
    }
}

exports.patchAcceptInterested = async (req, res, next) => {
    const { interestedId } = req.params;
    console.log(interestedId);
    try {
        const interested = await Interested.findByIdAndUpdate(interestedId, { accepted: true }, { new: true });
        res.status(200).json({ confirm: true, message: "Zaakceptowano zainteresowanie", interested });
    } catch (error) {
        res.status(409).json({ confirm: false, message: "Wystąpił błąd przy akceptowaniu zainteresowania" });
    }
}
