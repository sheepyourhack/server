const randomString = require('randomstring');
const sharp = require('sharp');
const sizeOf = require('image-size');
const PromiseFtp = require('promise-ftp');
const path = require('path');
const fs = require('fs');

exports.postAddImage = async (req, res, next) => {
    // random generate filename and add original file extension
    const fileName = `${randomString.generate()}.${req.file.originalname.split('.')[1]}`;

    try {
        if (sizeOf(req.file.buffer).width > 1920) { // if width is bigger then 1920 - resize image
            try {
                sharp(req.file.buffer)
                    .resize(1920)
                    .toBuffer()
                    .then(data => {
                        saveImage(data, fileName, res);
                    });
            } catch (err) {
                console.log(err);
                res.status(404).json({ confirm: false, message: "Wgrywanie zdjęcia nie powiodło się" });
            }
        } else { // if not - save image with original size
            try {
                const file = req.file.buffer;

                saveImage(file, fileName, res);
            } catch (err) {
                console.log(err);
                res.status(404).json({ confirm: false, message: "Wgrywanie zdjęcia nie powiodło się" });
            }
        }
    } catch (err) {
        console.log(err);
        res.status(404).json({ confirm: false, message: "Wgrywanie zdjęcia nie powiodło się" });
    }
}

exports.getImage = async (req, res, next) => {
    const { fileName } = req.params;

    getImage(fileName, res);
}

const saveImage = (file, fileName, res) => {
    const ftp = new PromiseFtp();
    fs.writeFile(path.join(__dirname, '..', 'upload', fileName), file, (err) => {
        if (err) {
            console.log(err);
            return res.status(409).json({ confirm: false, message: 'Błąd przesyłania plików' });
        }

        // send file to ftp server
        ftp.connect({ host: 'potaczek.smarthost.pl', user: 'potaczek', password: '1qw2DCVF', debug: console.log, forcePasv: true, })
            .then((serverMessage) => {
                return ftp.put(path.join(__dirname, '..', 'upload', fileName), `/images/${fileName}`);
            }).then(() => {
                // dedlete temp file
                console.log(fileName)
                fs.unlink(path.join(path.join(__dirname, '..', 'upload'), fileName), err => {
                    if (err) throw err;
                });
                ftp.end();
                res.status(200).json({ confirm: true, message: "Plik poprawnie przesłany", imagePath: fileName });
            });
    });
}

const getImage = (fileName, res) => {
    const ftp = new PromiseFtp(); 
    ftp.connect({ host: 'potaczek.smarthost.pl', user: 'potaczek', password: '1qw2DCVF', debug: console.log, forcePasv: true, })
        .then((serverMessage) => {
            return ftp.get(`/images/${fileName}`);
        }).then(function (stream) {
            return new Promise(function (resolve, reject) {
                stream.once('close', resolve);
                stream.once('error', reject);
                stream.pipe(fs.createWriteStream(fileName))
            });
        }).then(function () {
            res.sendFile(path.join(__dirname, '..', fileName), () => {
                fs.unlink(path.join(__dirname, '..', fileName), err => {
                    if (err) throw err;
                });
            });

            return ftp.end();
        });
}