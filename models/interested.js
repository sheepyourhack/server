const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const interestedSchema = new Schema({
    organizationId: {
        required: true,
        type: String
    },
    userId: {
        required: true,
        type: String
    },
    offerId: {
        required: true,
        type: String
    },
    date: {
        required: true,
        type: Date
    },
    accepted: {
        default: false,
        type: Boolean
    }
});

const Interested = mongoose.model('Interested', interestedSchema);

exports.Interested = Interested;