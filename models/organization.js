const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const organizationSchema = new Schema({
    email: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    name: {
        type: String,
        required: true
    },
    phone: String,
    foundingDate: Date,
    description: String,
    webpage: String,
    address: String,
    branch: String,
    specializations: [String],
    size: String,
    type: String,
    opinions: [Object],
    imagePath: String,
    unlockedUsers: [String]
});

const Organization = mongoose.model('Organization', organizationSchema);

exports.Organization = Organization;