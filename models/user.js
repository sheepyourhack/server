const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    birthDate: {
        type: Date,
        required: true
    },
    city: String,
    occupation: String,
    occupationPlace: String,
    description: String,
    skills: [Object],
    imagePath: String,
    experience: [Object],
    opinions: [Object],
    education: [Object],
    courses: [Object],
    hobbys: [String],
    phone: String,
    cvFile: String
});

const User = mongoose.model('User', userSchema);

exports.User = User;