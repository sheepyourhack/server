const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const offerSchema = new Schema({
    ownerName: {
        required: true,
        type: String
    },
    title: {
        required: true,
        type: String
    },
    ownerId: {
        required: true,
        type: String
    },
    type: {
        required: true,
        type: String
    },
    city: {
        required: true,
        type: String
    },
    paid: {
        type: Boolean,
        default: false
    },
    startDate: {
        required: true,
        type: Date
    },
    endDate: {
        required: true,
        type: Date
    },
    address: String,
    ownerType: String,
    requirements: [String],
    description: String,
    responsibilities: [String],
    website: String,
    imagePath: String
});

offerSchema.plugin(mongoosePaginate);

const Offer = mongoose.model('Offer', offerSchema);

exports.Offer = Offer;