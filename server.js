const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const PORT = process.env.PORT || 5000;
require('dotenv').config();

// import routes
const userRoutes = require('./routes/user');
const organizationRoutes = require('./routes/organization');
const authRoutes = require('./routes/auth');
const offerRoutes = require('./routes/offer');
const imageRoutes = require('./routes/image');
const interestedRoutes = require('./routes/interested');

// parse incoming request body to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// routes
app.use('/api/user', userRoutes);
app.use('/api/organization', organizationRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/offer', offerRoutes);
app.use('/api/image', imageRoutes);
app.use('/api/interested', interestedRoutes);

// connect to MongoDB
try {
    mongoose.connect(`mongodb+srv://${process.env.DATABASE_USER}:${process.env.DATABASE_PASSWORD}@cluster0-iofuv.mongodb.net/sheep-your-hack?retryWrites=true`, { useNewUrlParser: true });
    app.listen(PORT, '0.0.0.0', () => {
        console.log(`Server is running on PORT: ${PORT}`);
    });
} catch (err) {
    console.log(err);
}

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);