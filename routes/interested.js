const express = require('express');

const interestedController = require('../controllers/interestedController');

const router = express.Router();

router.post('/add', interestedController.postAddInterested);
router.patch('/accept/:interestedId', interestedController.patchAcceptInterested);

module.exports = router;