const express = require('express');

const authController = require('../controllers/authController');

const router = express.Router();

// loginW
router.post('/login', authController.postLogin);
router.post('/', authController.postTokenData);

module.exports = router;