const express = require('express');

const offerController = require('../controllers/offerController');

const router = express.Router();

router.post('/add', offerController.postAddOffer);
router.delete('/:offerId', offerController.deleteOffer);
router.post('/', offerController.postOffers);
router.post('/organization', offerController.postOrganizationOffers);
router.get('/:offerId', offerController.getOffer);

module.exports = router;