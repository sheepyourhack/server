const express = require('express');

const userController = require('../controllers/userController');

const router = express.Router();

// register new user
router.post('/add', userController.postAddUser);

// update user data
router.patch('/:userId', userController.patchUserData);

// delete user
router.delete('/:userId', userController.deleteUser);

// delete user
router.put('/opinions/:userId', userController.putComment);

// get user data
router.get('/:userId', userController.getUser);

// get user data
router.get('/', userController.getUsers);

module.exports = router;