const express = require('express');

const organizationController = require('../controllers/organizationController');

const router = express.Router();

// register new user
router.post('/add', organizationController.postAddOrganization);
router.patch('/:orgId', organizationController.patchOrganizationData);
router.delete('/:orgId', organizationController.deleteOrganization);
router.put('/opinions/:orgId', organizationController.putOrganizationOpinions);
router.get('/:orgId', organizationController.getOrganization);
router.get('/', organizationController.getOrganizations); 
module.exports = router;